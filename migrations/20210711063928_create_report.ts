import { Knex } from 'knex';
import { tables } from '../tables'

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable(tables.REPORT, (table) => {
    table.increments();
    table.string('video_filename');
    table.text('text');
    table.jsonb('hesitation_details');
    // table.jsonb('word2vec_details')
    // table.integer('user_id').unsigned().notNullable();
    // table.foreign('user_id').references('id').inTable('users');
    table.timestamps(false, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(tables.REPORT);
}
