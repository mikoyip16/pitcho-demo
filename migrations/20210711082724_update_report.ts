import { Knex } from 'knex';
import { tables } from '../tables';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable(tables.REPORT, (table) => {
    table.integer('end_of_speech_offset');
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable(tables.REPORT, (table) => {
    table.dropColumn('end_of_speech_offset');
  });
}
