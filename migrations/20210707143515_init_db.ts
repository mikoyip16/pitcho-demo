import { Knex } from "knex";

const traingTableName = "training";
export async function up(knex: Knex): Promise<void> {
    // create table users
    // id, username, password, created_at, updated_at
    await knex.schema.createTable(traingTableName, (table) => {
        table.increments("training_id"); // id
        table.text("data").notNullable();
        table.integer("label")
        table.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(traingTableName);
}
