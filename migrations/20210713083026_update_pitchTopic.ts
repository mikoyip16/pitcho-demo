import { Knex } from "knex";
import { tables } from "../tables"


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table(tables.PITCHTOPIC, table => {
        table.string('chinese_word');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.table(tables.PITCHTOPIC, table => {
        table.dropColumn('chinese_word');
    })
}

