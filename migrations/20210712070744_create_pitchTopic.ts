
import { Knex } from "knex";
import { tables } from "../tables"


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(tables.PITCHTOPIC, (table) => {
        table.increments("pitch_topic_id"); // id
        table.string("topic").notNullable();
        table.string("category").notNullable();
        table.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(tables.PITCHTOPIC);
}

