import { Knex } from 'knex';

const usersTableName = 'users';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable(usersTableName, (table) => {
    table.increments();
    table.string("username").unique().notNullable();
    table.string("password").notNullable();
    table.timestamps(false, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(usersTableName);
}
