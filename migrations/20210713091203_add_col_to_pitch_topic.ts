import { Knex } from "knex";
import { tables } from "../tables"



export async function up(knex: Knex): Promise<void> {
    await knex.schema.table(tables.PITCHTOPIC, table => {
        table.string('picture');
    })
    await knex.schema.alterTable(tables.PITCHTOPIC, (table) => {
        table.string('chinese_word').notNullable().alter();
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.table(tables.PITCHTOPIC, table => {
        table.dropColumn('picture');
    })

    await knex.schema.alterTable(tables.PITCHTOPIC, (table) => {
        table.string('chinese_word').nullable().alter();
    });
}