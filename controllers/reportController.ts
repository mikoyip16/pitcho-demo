import { ReportService } from '../services/reportService';
import { Request, Response } from 'express';

export class ReportController {
  constructor(private reportService: ReportService) {}

  getText = async (req: Request, res: Response) => {
    try {
      const videoID = req.session['video']['id']; // video.id = report.id

      const result = await this.reportService.getText(videoID);

      console.log(result);

      res.json(result[0]);
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  };

  getAllReport = async (req: Request, res: Response) => {
    try {
      const userID = req.session['user']['id'];

      const result = await this.reportService.getAllReport(userID);

      res.json(result);
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  };

  getPastReportSingle = async (req: Request, res: Response) => {
    try {
      const reportID = parseInt(req.params.id);
      const result = await this.reportService.getPastReportSingle(reportID);
      res.json(result[0]);
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  };


}
