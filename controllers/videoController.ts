import { VideoService } from '../services/VideoService';
import { Request, Response } from 'express';

export class VideoController {
  constructor(private videoService: VideoService) {}

  createVideo = async (req: Request, res: Response) => {
    try {
      const videoFileName = req.body.videoFileName;
      const topicID = req.session['topicID'].id;
      if (!videoFileName) {
        res.status(400).json({ message: 'invalid input' });
        return;
      }
      const insertedVideoID = await this.videoService.createVideo(videoFileName, topicID);

      req.session['video'] = {
        id: insertedVideoID,
        fileName: videoFileName,
      };

      res.json({ message: 'success', id: insertedVideoID });
      console.log('created video');
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };

  getVideo = async (req: Request, res: Response) => {
    try {
      const videoID = req.session['video']['id'];

      const foundVideo = await this.videoService.getVideo(videoID);
      res.json({ foundVideo });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };

  getPastVideo = async (req: Request, res: Response) => {
    try {
      const reportID = parseInt(req.params.id);
      const foundVideo = await this.videoService.getPastVideo(reportID)
      res.json({ foundVideo });

    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'internal server error' });

    }
  }
}
