import express from "express";
import { knnController } from "../main";

export const knnRoutes = express.Router();


knnRoutes.get("/", knnController.getTrainingData);
knnRoutes.post("/", knnController.postTrainingData);
