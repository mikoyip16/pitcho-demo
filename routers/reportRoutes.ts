import express from 'express';
import { reportController } from '../main';

export const reportRoutes = express.Router();

reportRoutes.get('/text', reportController.getText);
reportRoutes.get('/past_reports', reportController.getAllReport);
reportRoutes.get('/past_reports_single/:id', reportController.getPastReportSingle);

