const classifier = knnClassifier.create()


const trainingClass = Object.freeze({
    0: "touch Neck Left", 
    1: "touch Neck Right", 
    2: "arm Cross Front", 
    3: "arm Cross Back",
    4: "cross Legs Left", 
    5: "cross Legs Right", 
    6: "eyeglasses Left", 
    7: "eyeglasses Right", 
    8: "hair Left", 
    9: "hair Right", 
    10: "pocket Both", 
    11: "touch Forehead Left", 
    12: "touch Forehead Right", 
    13: "touchHand left touch right", 
    14: "touchHand right touch left", 
    15: "natual",
    16: "counting Left", 
    17: "counting Right", 
    18: "you left", 
    19: "you right",
    20: "you both",
    21: "main", 
    100: ":( 摸頭/面/頸 ＝ 可能感到不安", 
    101: ":( 叉手/摸手 ＝ 防衛性動作，可能想拉遠和別人的距離", 
    102: "^.^嘗試用手部動作配合演講，以清楚表達",
    103: "",
    104: ":( 手撫額頭 ＝ 可能感到掙扎或懊悔" 
});



async function setupClassifyData(){
    let dataFromDB = await getTrainingData()
    let pose_data = []
    let pose_label = []

    dataFromDB.trainingData.forEach((trainingTableElement) => {
        pose_data.push(JSON.parse(trainingTableElement.data))
        pose_label.push(trainingTableElement.label)
    })
    if (pose_label != null)
        for (let i = 0; i < pose_label.length; i++) {
            let label = pose_label[i]
            let data = []
            for (let d of pose_data[i]) {
                data.push(d.x)
                data.push(d.y)
                data.push(d.z)
                data.push(d.visibility)
            }
            data = tf.tensor(data)
            data.reshape([-1])
            // console.log(pose_data[i])
            classifier.addExample(data, label)
        }

}





async function classify(lastPoseLandmarks) {
    let data = []
    for (let d of lastPoseLandmarks) {
        data.push(d.x)
        data.push(d.y)
        data.push(d.z)
        data.push(d.visibility)
    }
    data = tf.tensor(data)
    data.reshape([-1])
    const res = await classifier.predictClass(data, 3)

    resArray = Object.entries(res.confidences)

    let gpPredictedClass;
        if (parseInt(res.label) == 0 || parseInt(res.label) == 1 || parseInt(res.label) == 6 || parseInt(res.label) == 7 || parseInt(res.label) == 8 || parseInt(res.label) == 9) {
            gpPredictedClass = 100;
        } else if (parseInt(res.label) == 2 || parseInt(res.label) == 3 || parseInt(res.label) == 13 || parseInt(res.label) == 14) {
            gpPredictedClass = 101;
        } else if (parseInt(res.label) == 21) {
            gpPredictedClass = 102;
        } else if ((parseInt(res.label) == 11 || (parseInt(res.label) == 12))) {
            gpPredictedClass = 104;
        } else gpPredictedClass = 103;
    
    const predictedResult = trainingClass[gpPredictedClass]
    document.querySelector(
        '#predict'
    ).innerHTML = `${predictedResult}`
    return `${predictedResult}`
}

async function getTrainingData() {
    const res = await fetch('/knn', {
        headers: {
            'Content-Type': 'application/json',
        },
    })
    if (res.status === 200) {
        let data = await res.json()
        console.log(data)
        return data
    } else {
        let err = await res.json()
        console.log(err)
    }
}
