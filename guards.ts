import { Request, Response, NextFunction } from 'express';
import path from 'path';

export const isLoggedIn = (req: Request, res: Response, next: NextFunction) => {
  const user = req.session['user'];
  if (!user) {
    console.log('not yet login');
    // res.redirect("/index.html");
    res.sendFile(path.join(__dirname, './public/index.html'));
  } else {
    next();
  }
};
