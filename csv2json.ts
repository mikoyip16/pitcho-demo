import xlsx from "xlsx";
// import Knex from "knex";
// import * as knexConfig from "./knexfile";
import dotenv from "dotenv";

dotenv.config();

// interface UserData {
//     username: string;
//     password: string;
//     level: string;
//     //   jasonIsHandsome: string;
// }

interface TrainingData { //read file果時用，所以跟返excel 入面既內容，而唔係跟db, eg. is_file ( excel係number, db係boolean), owner (excel係string, db係number)
    training_id: number;
    data: string;
    label: number;
    created_at: number;
    updated_at: number;
}

// interface CategoryData {
//     name: string;
// }
const workbook = xlsx.readFile("training.csv");

console.log(workbook.SheetNames);
export const trainingData = readSheetData<TrainingData>(workbook, "Sheet1");
// console.log(userData);
console.log(trainingData[0].label);

function readSheetData<T>(workbook: xlsx.WorkBook, sheetName: string) {
    const sheet = workbook.Sheets[sheetName];
    const data = xlsx.utils.sheet_to_json<T>(sheet);
    return data;
}

// async function runImport() {
//     const workbook = xlsx.readFile("training.csv");

//     console.log(workbook.SheetNames);
//     const userData = readSheetData(workbook, "Sheet1");
//     userData[0].level
//     const fileData = readSheetData(workbook, "file");
//     const categoryData = readSheetData(workbook, "category");

//     console.log(userData[0]);
//     console.log(fileData[0]);
//     console.log(categoryData[0]);

//     const distinctOwnerSet = fileData.reduce((set, file) => { //Set = 唔可以重覆既array, 搵返excel入面所有owner names
//         set.add(file.owner);
//         return set;
//     }, new Set());

//     // const set = new Set([1, 2, 3, 4]);
//     // set.add(1) // [1,2,3,4]
//     // set.has(1) 
//     // set.add(5) // [1,2,3,4,5]

//     console.log(distinctOwnerSet);
//     const CORRECT_USER_MAPPING = Object.freeze({
//         gordan: "gordon",
//         alexs: "alex",
//         admiin: "admin",
//         ales: "alex",
//         micheal: "michael",
//     });

//     const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
//     const tableNameUsers = "users";
//     const tableNameFiles = "files";
//     const tableNameCategories = "categories";

//     try {
//         // DELETE FROM TABLES
//         await knex(tableNameFiles).del();
//         await knex(tableNameCategories).del();
//         await knex(tableNameUsers).del();

//         // INSERT DATA INTO users
//         //[1,2,3,4].reduce((acc,cur)=>acc + cur,0)
//         //acc -> Mapping, 
//         //cur => insertedUser
//         // ,0 => new Map<string, number>()

//         // let mapping = new Map<string, number>()
//         // for (const insertedUser of (await knex(tableNameUsers).insert(userData).returning(["username", "id"])) as { username: string; id: number }[]) {
//         //     mapping = mapping.set(insertedUser.username, insertedUser.id)
//         // }
//         // console.log(mapping)

//         const reducer = (mapping: Map<string, number>, insertedUser: { username: string; id: number }) => {
//             mapping.set(insertedUser.username, insertedUser.id);
//             return mapping;
//         }
//         // userMapping 個樣類似： 
//         // {
//         //     alex:1,
//         //     admin:2
//         // }
//         const userMapping: Map<string, number> = (
//             await knex(tableNameUsers).insert(userData).returning(["username", "id"]) //excel column name = db column name 就可以直接.insert, 但因為excel裡面file sheet入面owner係string, 而家db要用id代表，所以要returning + map
//         ).reduce(reducer, new Map<string, number>());

//         // INSERT DATA INTO categories
//         const insertedCategories: Array<{ name: string; id: number }> = await knex(
//             tableNameCategories
//         )
//             .insert(categoryData)
//             .returning(["id", "name"]);
//         const categoryMapping = insertedCategories.reduce(
//             (mapping, insertedCategory) => {
//                 mapping.set(insertedCategory.name, insertedCategory.id);
//                 return mapping;
//             },
//             new Map<string, number>()
//         );
//         // console.log(categoryMapping);

//         const toBeInsertFileData = fileData.map((file) => ({ //{}最外加()因為如果整{}會以為入面係code, 因為function 係()=>{}, 但而家想output個object, 用個（）包住佢想講真係要個object
//             name: file.name,
//             content: file.Content,
//             is_file: file.is_file, //1true 0false 唔洗特登搞
//             category_id: categoryMapping.get(file.category),
//             owner_id:
//                 userMapping.get(file.owner) || //係Map果到用username黎搵返個id, 而家user sheet既user name 無錯，但係file sheet's owner name有錯，用(file.owner)咁樣搵，錯既owner name係搵唔到id
//                 userMapping.get(CORRECT_USER_MAPPING[file.owner]), //  如果(file.owner)係undefined 就得後面果個(CORRECT_USER_MAPPING[file.owner])，用錯既名搵返岩既名，再搵返個id
//         }));
//         console.log(CORRECT_USER_MAPPING)
//         console.log(toBeInsertFileData[89]);
//         await knex.batchInsert(tableNameFiles, toBeInsertFileData, 30); //batchInset就30 30條塞入去, 如果頭30個已爆error就只會爆30個出黎睇
//         // await knex(tableNameFiles).insert(toBeInsertFileData) 全部一齊insert, 寫法有D唔同

//         console.log("done !!!");
//     } catch (err) {
//         console.error(err.message);
//     }

//     await knex.destroy();
// }

// runImport();
