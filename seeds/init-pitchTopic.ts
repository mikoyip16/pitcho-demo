import { Knex } from 'knex';

const topicTableName = 'pitch_topic';
const workCategory = 'WORK';
const familyCategory = 'FAMILY';
const lifeCategory = 'LIFE';
const moneyCategory = 'MONEY';
const aiCategory = 'AI';
const generderEqualityCategory = 'GENDER EQUALITY';
const bossCategory = 'BOSS';
const travelCategory = 'TRAVEL';

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex(topicTableName).del();

  // Inserts seed entries
  await knex(topicTableName).insert([
    {
      topic: '如何利用Pitcho提升你的溝通技巧？',
      category: workCategory,
      chinese_word: '溝通',
      picture: 'work1.jpg',
    },
    {
      topic: '如何有效管理一個工作團隊？',
      category: workCategory,
      chinese_word: '溝通',
      picture: 'work2.jpg',
    },
    {
      topic: '職場上的「過客」、「囚犯」、「抱怨者」和「選手」',
      category: workCategory,
      chinese_word: '溝通',
      picture: 'work3.jpg',
    },

    {
      topic: '你結婚後會和父母一起住嗎? 為什麼?',
      category: familyCategory,
      chinese_word: '家庭',
      picture: 'family1.PNG',
    },
    {
      topic: '你會想要教你的孩子什麼事呢?',
      category: familyCategory,
      chinese_word: '家庭',
      picture: 'family2.PNG',
    },
    {
      topic: '請說說看你認為的理想家庭是如何。',
      category: familyCategory,
      chinese_word: '家庭',
      picture: 'family3.PNG',
    },

    {
      topic: '你的人生待辦清單裡有什麼呢? 為什麼?',
      category: lifeCategory,
      chinese_word: '人生',
      picture: 'life1.PNG',
    },
    {
      topic: '你的人生目標是什麼?',
      category: lifeCategory,
      chinese_word: '人生',
      picture: 'life2.PNG',
    },
    {
      topic: '請說說看目前發生在你人生中最大的事情。',
      category: lifeCategory,
      chinese_word: '人生',
      picture: 'life3.PNG',
    },

    {
      topic: '你認為世界上的人是否過於擔心錢的問題呢? 那是為什麼?',
      category: moneyCategory,
      chinese_word: '金錢',
      picture: 'money1.PNG',
    },
    {
      topic: '在你所處的社會或環境，金錢會引起什麼樣的問題呢?',
      category: moneyCategory,
      chinese_word: '金錢',
      picture: 'money2.PNG',
    },
    {
      topic: '你認為人沒有錢能生存嗎? 為什麼?',
      category: moneyCategory,
      chinese_word: '金錢',
      picture: 'money3.PNG',
    },

    {
      topic: '請說說看你身邊有什麼樣的AI，其功能又是為何。',
      category: aiCategory,
      chinese_word: '人工智能',
      picture: 'ai1.PNG',
    },
    {
      topic: '比利時有醫院是機器人負責櫃台工作。你認為將來AI代替人類的話，會從事什麼樣的工作呢?',
      category: aiCategory,
      chinese_word: '人工智能',
      picture: 'ai2.PNG',
    },
    {
      topic: '你認為AI在所有方面都可以超越人類嗎? 為什麼呢?',
      category: aiCategory,
      chinese_word: '人工智能',
      picture: 'ai3.PNG',
    },

    {
      topic: '從權利和義務的面向考量的話，你認為你的國家有什麼性別歧視嗎?',
      category: generderEqualityCategory,
      chinese_word: '男女平等',
      picture: 'genderEquality1.PNG',
    },
    {
      topic: '在你的國家，約會中的男性會有幫女性買單的傾向嗎? 你對於此事是贊成還是反對呢?',
      category: generderEqualityCategory,
      chinese_word: '男女平等',
      picture: 'genderEquality2.PNG',
    },
    {
      topic: '為了落實真正的男女平等，你認為應該改變什麼呢?',
      category: generderEqualityCategory,
      chinese_word: '男女平等',
      picture: 'genderEquality3.PNG',
    },

    {
      topic: '你認為什麼樣的人是理想的上司呢?',
      category: bossCategory,
      chinese_word: '上司',
      picture: 'boss1.PNG',
    },
    {
      topic: '你知道什麼關於職場騷擾的事嗎? 請試著討論看看職場騷擾。',
      category: bossCategory,
      chinese_word: '上司',
      picture: 'boss2.PNG',
    },
    {
      topic: '即使是同樣的憤怒行為，你認為對接受者來說，感性的憤怒和理性憤怒有什麼樣的不同?',
      category: bossCategory,
      chinese_word: '上司',
      picture: 'boss3.PNG',
    },

    {
      topic: '你在旅行前會調查目的地的評價嗎? 為什麼?',
      category: travelCategory,
      chinese_word: '旅遊',
      picture: 'travelq1.PNG',
    },
    {
      topic: '您下個月要去巴黎。跟團旅行和自助旅行哪個比較好呢?',
      category: travelCategory,
      chinese_word: '旅遊',
      picture: 'travelq2.PNG',
    },
    {
      topic: '你是你故鄉的觀光大使。請介紹故鄉值得一去的地方。',
      category: travelCategory,
      chinese_word: '旅遊',
      picture: 'travelq3.PNG',
    },
  ]);
}
