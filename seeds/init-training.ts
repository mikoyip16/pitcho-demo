import { Knex } from "knex";
import { trainingData } from "../csv2json";

const traingTableName = "training";


export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(traingTableName).del();

    // Inserts seed entries
    for (const eachTrainingData of trainingData) {
        await knex(traingTableName).insert(
            { data: eachTrainingData.data, label: eachTrainingData.label }
        );
    }

};
