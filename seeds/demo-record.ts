import { Knex } from 'knex';
import { tables } from '../tables';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(tables.REPORT).del();

    // Inserts seed entries
    await knex(tables.REPORT).insert([{
        video_filename: '1626603558412',
        hesitation_details: JSON.stringify([
            {
                ITN: '大家 有冇 睇過 皇上 無話 兒 呢套 戲 呢 佢 咧 係 講述 英女王 嘅 父親 呢 係 有 嚴重 嘅 口吃 啦 但系 因為 咧 要 登記 做 國王 所以 咧 要 克服 口吃 最後 真係 成功 咗 仲 周圍 去 發表 演說 嘅 一個 故事 嚟嘅',
                Words: [
                    {
                        Word: '大家',
                        Offset: 71800000,
                        Duration: 3300000,
                    },
                    {
                        Word: '有冇',
                        Offset: 75200000,
                        Duration: 2500000,
                    },
                    {
                        Word: '睇過',
                        Offset: 77800000,
                        Duration: 3900000,
                    },
                    {
                        Word: '皇上',
                        Offset: 81800000,
                        Duration: 4900000,
                    },
                    {
                        Word: '無話',
                        Offset: 86800000,
                        Duration: 3500000,
                    },
                    {
                        Word: '兒',
                        Offset: 90400000,
                        Duration: 1900000,
                    },
                    {
                        Word: '呢套',
                        Offset: 92400000,
                        Duration: 2900000,
                    },
                    {
                        Word: '戲',
                        Offset: 95400000,
                        Duration: 2000000,
                    },
                    {
                        Word: '呢',
                        Offset: 97500000,
                        Duration: 5000000,
                    },
                    {
                        Word: '佢',
                        Offset: 102800000,
                        Duration: 2900000,
                    },
                    {
                        Word: '咧',
                        Offset: 105800000,
                        Duration: 1500000,
                    },
                    {
                        Word: '係',
                        Offset: 107400000,
                        Duration: 2500000,
                    },
                    {
                        Word: '講述',
                        Offset: 110000000,
                        Duration: 5100000,
                    },
                    {
                        Word: '英女王',
                        Offset: 115200000,
                        Duration: 7100000,
                    },
                    {
                        Word: '嘅',
                        Offset: 122400000,
                        Duration: 2500000,
                    },
                    {
                        Word: '父親',
                        Offset: 125000000,
                        Duration: 4700000,
                    },
                    {
                        Word: '呢',
                        Offset: 129800000,
                        Duration: 1500000,
                    },
                    {
                        Word: '係',
                        Offset: 131400000,
                        Duration: 2500000,
                    },
                    {
                        Word: '有',
                        Offset: 134000000,
                        Duration: 2700000,
                    },
                    {
                        Word: '嚴重',
                        Offset: 136800000,
                        Duration: 4800000,
                    },
                    {
                        Word: '嘅',
                        Offset: 141700000,
                        Duration: 800000,
                    },
                    {
                        Word: '口吃',
                        Offset: 142600000,
                        Duration: 4400000,
                    },
                    {
                        Word: '啦',
                        Offset: 147100000,
                        Duration: 3800000,
                    },
                    {
                        Word: '但系',
                        Offset: 151200000,
                        Duration: 3200000,
                    },
                    {
                        Word: '因為',
                        Offset: 154500000,
                        Duration: 3200000,
                    },
                    {
                        Word: '咧',
                        Offset: 157800000,
                        Duration: 2300000,
                    },
                    {
                        Word: '要',
                        Offset: 160200000,
                        Duration: 2700000,
                    },
                    {
                        Word: '登記',
                        Offset: 163000000,
                        Duration: 4700000,
                    },
                    {
                        Word: '做',
                        Offset: 167800000,
                        Duration: 1900000,
                    },
                    {
                        Word: '國王',
                        Offset: 169800000,
                        Duration: 4900000,
                    },
                    {
                        Word: '所以',
                        Offset: 174800000,
                        Duration: 2700000,
                    },
                    {
                        Word: '咧',
                        Offset: 177600000,
                        Duration: 1900000,
                    },
                    {
                        Word: '要',
                        Offset: 179600000,
                        Duration: 1900000,
                    },
                    {
                        Word: '克服',
                        Offset: 181600000,
                        Duration: 4500000,
                    },
                    {
                        Word: '口吃',
                        Offset: 186200000,
                        Duration: 5100000,
                    },
                    {
                        Word: '最後',
                        Offset: 191400000,
                        Duration: 3600000,
                    },
                    {
                        Word: '真係',
                        Offset: 195100000,
                        Duration: 3000000,
                    },
                    {
                        Word: '成功',
                        Offset: 198200000,
                        Duration: 4300000,
                    },
                    {
                        Word: '咗',
                        Offset: 202600000,
                        Duration: 2100000,
                    },
                    {
                        Word: '仲',
                        Offset: 204800000,
                        Duration: 2100000,
                    },
                    {
                        Word: '周圍',
                        Offset: 207000000,
                        Duration: 4900000,
                    },
                    {
                        Word: '去',
                        Offset: 212000000,
                        Duration: 1700000,
                    },
                    {
                        Word: '發表',
                        Offset: 213800000,
                        Duration: 3300000,
                    },
                    {
                        Word: '演說',
                        Offset: 217200000,
                        Duration: 5100000,
                    },
                    {
                        Word: '嘅',
                        Offset: 222400000,
                        Duration: 1100000,
                    },
                    {
                        Word: '一個',
                        Offset: 223600000,
                        Duration: 2100000,
                    },
                    {
                        Word: '故事',
                        Offset: 225800000,
                        Duration: 4500000,
                    },
                    {
                        Word: '嚟嘅',
                        Offset: 230400000,
                        Duration: 6800000,
                    },
                ],
                Display:
                    '大家有冇睇過皇上無話兒呢套戲呢？佢咧係講述英女王嘅父親呢？係有嚴重嘅口吃啦，但系因為咧，要登記做國王，所以咧？要克服口吃，最後真係成功咗仲周圍去發表演說嘅一個故事嚟嘅。',
                Lexical:
                    '大家 有冇 睇過 皇上 無話 兒 呢套 戲 呢 佢 咧 係 講述 英女王 嘅 父親 呢 係 有 嚴重 嘅 口吃 啦 但系 因為 咧 要 登記 做 國王 所以 咧 要 克服 口吃 最後 真係 成功 咗 仲 周圍 去 發表 演說 嘅 一個 故事 嚟嘅',
                MaskedITN:
                    '大家有冇睇過皇上無話兒呢套戲呢佢咧係講述英女王嘅父親呢係有嚴重嘅口吃啦但系因為咧要登記做國王所以咧要克服口吃最後真係成功咗仲周圍去發表演說嘅一個故事嚟嘅',
                Confidence: 0.67287785,
            },
            {
                ITN: '你 可能 咧 冇 口吃 但系 都會 想 提升 自己 嘅 表達 能力',
                Words: [
                    {
                        Word: '你',
                        Offset: 251400000,
                        Duration: 3000000,
                    },
                    {
                        Word: '可能',
                        Offset: 254500000,
                        Duration: 3700000,
                    },
                    {
                        Word: '咧',
                        Offset: 258300000,
                        Duration: 2400000,
                    },
                    {
                        Word: '冇',
                        Offset: 260800000,
                        Duration: 2300000,
                    },
                    {
                        Word: '口吃',
                        Offset: 263200000,
                        Duration: 5300000,
                    },
                    {
                        Word: '但系',
                        Offset: 268600000,
                        Duration: 2300000,
                    },
                    {
                        Word: '都會',
                        Offset: 271000000,
                        Duration: 2500000,
                    },
                    {
                        Word: '想',
                        Offset: 273600000,
                        Duration: 2900000,
                    },
                    {
                        Word: '提升',
                        Offset: 276600000,
                        Duration: 5100000,
                    },
                    {
                        Word: '自己',
                        Offset: 281800000,
                        Duration: 2700000,
                    },
                    {
                        Word: '嘅',
                        Offset: 284600000,
                        Duration: 1200000,
                    },
                    {
                        Word: '表達',
                        Offset: 285900000,
                        Duration: 4000000,
                    },
                    {
                        Word: '能力',
                        Offset: 290000000,
                        Duration: 4500000,
                    },
                ],
                Display: '你可能咧，冇口吃，但系都會想提升自己嘅表達能力。',
                Lexical: '你 可能 咧 冇 口吃 但系 都會 想 提升 自己 嘅 表達 能力',
                MaskedITN: '你可能咧冇口吃但系都會想提升自己嘅表達能力',
                Confidence: 0.6728779,
            },
            {
                ITN: '但係 咧 有冇 咩 工具 幫到 收穫 所以 我哋 咧 就 開發 咗 呢一 個 工具 啦 你 可以 喺 手機 上面 咧 隨時隨地 咁樣 揀 題目 去 練習 啦 我哋嘅 系統 咧 會 即時 幫 你 錄影 然後 呢 分析 翻 嗰 條 片 嚟 入邊 嘅 肢體語言 同埋 語調 針對性 咁 比 返 啲 數據 嚟 咧 去 自己 參考 然之后 再 多啲 去 練習 嘅 隨時隨地 咁樣 練 呢',
                Words: [
                    {
                        Word: '但係',
                        Offset: 302600000,
                        Duration: 3900000,
                    },
                    {
                        Word: '咧',
                        Offset: 306600000,
                        Duration: 1500000,
                    },
                    {
                        Word: '有冇',
                        Offset: 308200000,
                        Duration: 2700000,
                    },
                    {
                        Word: '咩',
                        Offset: 311000000,
                        Duration: 1500000,
                    },
                    {
                        Word: '工具',
                        Offset: 312600000,
                        Duration: 4700000,
                    },
                    {
                        Word: '幫到',
                        Offset: 317400000,
                        Duration: 3900000,
                    },
                    {
                        Word: '收穫',
                        Offset: 321400000,
                        Duration: 7500000,
                    },
                    {
                        Word: '所以',
                        Offset: 329200000,
                        Duration: 4100000,
                    },
                    {
                        Word: '我哋',
                        Offset: 333400000,
                        Duration: 2900000,
                    },
                    {
                        Word: '咧',
                        Offset: 336400000,
                        Duration: 3100000,
                    },
                    {
                        Word: '就',
                        Offset: 339800000,
                        Duration: 1500000,
                    },
                    {
                        Word: '開發',
                        Offset: 341400000,
                        Duration: 4700000,
                    },
                    {
                        Word: '咗',
                        Offset: 346200000,
                        Duration: 1500000,
                    },
                    {
                        Word: '呢一',
                        Offset: 347800000,
                        Duration: 2700000,
                    },
                    {
                        Word: '個',
                        Offset: 350600000,
                        Duration: 1300000,
                    },
                    {
                        Word: '工具',
                        Offset: 352000000,
                        Duration: 5100000,
                    },
                    {
                        Word: '啦',
                        Offset: 357200000,
                        Duration: 3900000,
                    },
                    {
                        Word: '你',
                        Offset: 361400000,
                        Duration: 2800000,
                    },
                    {
                        Word: '可以',
                        Offset: 364300000,
                        Duration: 2600000,
                    },
                    {
                        Word: '喺',
                        Offset: 367000000,
                        Duration: 2100000,
                    },
                    {
                        Word: '手機',
                        Offset: 369200000,
                        Duration: 5000000,
                    },
                    {
                        Word: '上面',
                        Offset: 374300000,
                        Duration: 3400000,
                    },
                    {
                        Word: '咧',
                        Offset: 377800000,
                        Duration: 5300000,
                    },
                    {
                        Word: '隨時隨地',
                        Offset: 383400000,
                        Duration: 8100000,
                    },
                    {
                        Word: '咁樣',
                        Offset: 391600000,
                        Duration: 3100000,
                    },
                    {
                        Word: '揀',
                        Offset: 394800000,
                        Duration: 3100000,
                    },
                    {
                        Word: '題目',
                        Offset: 398000000,
                        Duration: 3500000,
                    },
                    {
                        Word: '去',
                        Offset: 401600000,
                        Duration: 1700000,
                    },
                    {
                        Word: '練習',
                        Offset: 403400000,
                        Duration: 5300000,
                    },
                    {
                        Word: '啦',
                        Offset: 408800000,
                        Duration: 4100000,
                    },
                    {
                        Word: '我哋嘅',
                        Offset: 413200000,
                        Duration: 4900000,
                    },
                    {
                        Word: '系統',
                        Offset: 418200000,
                        Duration: 4100000,
                    },
                    {
                        Word: '咧',
                        Offset: 422400000,
                        Duration: 1700000,
                    },
                    {
                        Word: '會',
                        Offset: 424200000,
                        Duration: 2700000,
                    },
                    {
                        Word: '即時',
                        Offset: 427000000,
                        Duration: 4100000,
                    },
                    {
                        Word: '幫',
                        Offset: 431200000,
                        Duration: 1600000,
                    },
                    {
                        Word: '你',
                        Offset: 432900000,
                        Duration: 1200000,
                    },
                    {
                        Word: '錄影',
                        Offset: 434200000,
                        Duration: 5100000,
                    },
                    {
                        Word: '然後',
                        Offset: 439400000,
                        Duration: 4100000,
                    },
                    {
                        Word: '呢',
                        Offset: 443600000,
                        Duration: 1500000,
                    },
                    {
                        Word: '分析',
                        Offset: 445200000,
                        Duration: 4600000,
                    },
                    {
                        Word: '翻',
                        Offset: 449900000,
                        Duration: 2200000,
                    },
                    {
                        Word: '嗰',
                        Offset: 452200000,
                        Duration: 1100000,
                    },
                    {
                        Word: '條',
                        Offset: 453400000,
                        Duration: 1900000,
                    },
                    {
                        Word: '片',
                        Offset: 455400000,
                        Duration: 3900000,
                    },
                    {
                        Word: '嚟',
                        Offset: 459400000,
                        Duration: 1600000,
                    },
                    {
                        Word: '入邊',
                        Offset: 461100000,
                        Duration: 4000000,
                    },
                    {
                        Word: '嘅',
                        Offset: 465200000,
                        Duration: 2100000,
                    },
                    {
                        Word: '肢體語言',
                        Offset: 467400000,
                        Duration: 8300000,
                    },
                    {
                        Word: '同埋',
                        Offset: 476000000,
                        Duration: 3300000,
                    },
                    {
                        Word: '語調',
                        Offset: 479400000,
                        Duration: 7700000,
                    },
                    {
                        Word: '針對性',
                        Offset: 487400000,
                        Duration: 6500000,
                    },
                    {
                        Word: '咁',
                        Offset: 494000000,
                        Duration: 1300000,
                    },
                    {
                        Word: '比',
                        Offset: 495400000,
                        Duration: 1100000,
                    },
                    {
                        Word: '返',
                        Offset: 496600000,
                        Duration: 1500000,
                    },
                    {
                        Word: '啲',
                        Offset: 498200000,
                        Duration: 1300000,
                    },
                    {
                        Word: '數據',
                        Offset: 499600000,
                        Duration: 4700000,
                    },
                    {
                        Word: '嚟',
                        Offset: 504400000,
                        Duration: 1700000,
                    },
                    {
                        Word: '咧',
                        Offset: 506200000,
                        Duration: 1300000,
                    },
                    {
                        Word: '去',
                        Offset: 507600000,
                        Duration: 2300000,
                    },
                    {
                        Word: '自己',
                        Offset: 510000000,
                        Duration: 3900000,
                    },
                    {
                        Word: '參考',
                        Offset: 514000000,
                        Duration: 7500000,
                    },
                    {
                        Word: '然之后',
                        Offset: 521800000,
                        Duration: 4500000,
                    },
                    {
                        Word: '再',
                        Offset: 526400000,
                        Duration: 3500000,
                    },
                    {
                        Word: '多啲',
                        Offset: 530000000,
                        Duration: 4500000,
                    },
                    {
                        Word: '去',
                        Offset: 534600000,
                        Duration: 1300000,
                    },
                    {
                        Word: '練習',
                        Offset: 536000000,
                        Duration: 4900000,
                    },
                    {
                        Word: '嘅',
                        Offset: 541000000,
                        Duration: 6300000,
                    },
                    {
                        Word: '隨時隨地',
                        Offset: 547600000,
                        Duration: 8700000,
                    },
                    {
                        Word: '咁樣',
                        Offset: 556400000,
                        Duration: 2500000,
                    },
                    {
                        Word: '練',
                        Offset: 559000000,
                        Duration: 1600000,
                    },
                    {
                        Word: '呢',
                        Offset: 560700000,
                        Duration: 4200000,
                    },
                ],
                Display:
                    '但係咧有冇咩工具幫到收穫，所以我哋咧就開發咗呢一個工具啦。你可以喺手機上面咧，隨時隨地咁樣揀題目去練習啦。我哋嘅系統咧會即時幫你錄影，然後呢？分析翻嗰條片嚟入邊嘅肢體語言同埋語調針對性咁比返啲數據嚟咧，去自己參考，然之后再多啲去練習嘅，隨時隨地咁樣練呢。',
                Lexical:
                    '但係 咧 有冇 咩 工具 幫到 收穫 所以 我哋 咧 就 開發 咗 呢一 個 工具 啦 你 可以 喺 手機 上面 咧 隨時隨地 咁樣 揀 題目 去 練習 啦 我哋嘅 系統 咧 會 即時 幫 你 錄影 然後 呢 分析 翻 嗰 條 片 嚟 入邊 嘅 肢體語言 同埋 語調 針對性 咁 比 返 啲 數據 嚟 咧 去 自己 參考 然之后 再 多啲 去 練習 嘅 隨時隨地 咁樣 練 呢',
                MaskedITN:
                    '但係咧有冇咩工具幫到收穫所以我哋咧就開發咗呢一個工具啦你可以喺手機上面咧隨時隨地咁樣揀題目去練習啦我哋嘅系統咧會即時幫你錄影然後呢分析翻嗰條片嚟入邊嘅肢體語言同埋語調針對性咁比返啲數據嚟咧去自己參考然之后再多啲去練習嘅隨時隨地咁樣練呢',
                Confidence: 0.6728778,
            },
            {
                ITN: '進步 得 快 d 聲效 到 大 啲 喇',
                Words: [
                    {
                        Word: '進步',
                        Offset: 572400000,
                        Duration: 4100000,
                    },
                    {
                        Word: '得',
                        Offset: 576600000,
                        Duration: 1500000,
                    },
                    {
                        Word: '快',
                        Offset: 578200000,
                        Duration: 1700000,
                    },
                    {
                        Word: 'd',
                        Offset: 580000000,
                        Duration: 2500000,
                    },
                    {
                        Word: '聲效',
                        Offset: 582600000,
                        Duration: 4200000,
                    },
                    {
                        Word: '到',
                        Offset: 586900000,
                        Duration: 1200000,
                    },
                    {
                        Word: '大',
                        Offset: 588200000,
                        Duration: 1700000,
                    },
                    {
                        Word: '啲',
                        Offset: 590000000,
                        Duration: 1100000,
                    },
                    {
                        Word: '喇',
                        Offset: 591200000,
                        Duration: 4800000,
                    },
                ],
                Display: '進步得快，D聲效到大啲喇。',
                Lexical: '進步 得 快 d 聲效 到 大 啲 喇',
                MaskedITN: '進步得快d聲效到大啲喇',
                Confidence: 0.67287785,
            },
        ]),
        end_of_speech_offset: 606600000,
        user_id: 1,
        topic_id: 1
    }, {
        video_filename: '1626808380088',
        hesitation_details: JSON.stringify([
            {
                "ITN": "我哋 呢個 網站 咧 其實 係 利用 咗 人工智能 入邊 嘅 機器 學習 去 幫 我哋 進行 一個 肢體語言 嘅 識別 嘅 咁 我哋 點解 要 咁 做 喇 因為 已經 有太 多 太多 嘅 研究 顯示 出 咧 演講者 嘅 非 言語 表達 咧 其實 嘅 重要性 呢 係 畀 語調 啦 甚至 我哋 一般 以為 嘅 演講 內容 嚟 得 更為 重要 嘅 咁 我哋 識別 到 啲 咩 咧 譬如 自然 狀態 勒",
                "Words": [
                    {
                        "Word": "我哋",
                        "Offset": 16000000,
                        "Duration": 4900000
                    },
                    {
                        "Word": "呢個",
                        "Offset": 21000000,
                        "Duration": 2500000
                    },
                    {
                        "Word": "網站",
                        "Offset": 23600000,
                        "Duration": 4100000
                    },
                    {
                        "Word": "咧",
                        "Offset": 27800000,
                        "Duration": 900000
                    },
                    {
                        "Word": "其實",
                        "Offset": 28800000,
                        "Duration": 3500000
                    },
                    {
                        "Word": "係",
                        "Offset": 32400000,
                        "Duration": 1900000
                    },
                    {
                        "Word": "利用",
                        "Offset": 34400000,
                        "Duration": 3700000
                    },
                    {
                        "Word": "咗",
                        "Offset": 38200000,
                        "Duration": 1500000
                    },
                    {
                        "Word": "人工智能",
                        "Offset": 39800000,
                        "Duration": 8300000
                    },
                    {
                        "Word": "入邊",
                        "Offset": 48200000,
                        "Duration": 3300000
                    },
                    {
                        "Word": "嘅",
                        "Offset": 51600000,
                        "Duration": 1900000
                    },
                    {
                        "Word": "機器",
                        "Offset": 53600000,
                        "Duration": 5100000
                    },
                    {
                        "Word": "學習",
                        "Offset": 58800000,
                        "Duration": 5100000
                    },
                    {
                        "Word": "去",
                        "Offset": 64000000,
                        "Duration": 1500000
                    },
                    {
                        "Word": "幫",
                        "Offset": 65600000,
                        "Duration": 2300000
                    },
                    {
                        "Word": "我哋",
                        "Offset": 68000000,
                        "Duration": 2700000
                    },
                    {
                        "Word": "進行",
                        "Offset": 70800000,
                        "Duration": 3400000
                    },
                    {
                        "Word": "一個",
                        "Offset": 74300000,
                        "Duration": 2200000
                    },
                    {
                        "Word": "肢體語言",
                        "Offset": 76600000,
                        "Duration": 6700000
                    },
                    {
                        "Word": "嘅",
                        "Offset": 83400000,
                        "Duration": 1500000
                    },
                    {
                        "Word": "識別",
                        "Offset": 85000000,
                        "Duration": 3800000
                    },
                    {
                        "Word": "嘅",
                        "Offset": 88900000,
                        "Duration": 2600000
                    },
                    {
                        "Word": "咁",
                        "Offset": 91800000,
                        "Duration": 2700000
                    },
                    {
                        "Word": "我哋",
                        "Offset": 94600000,
                        "Duration": 3200000
                    },
                    {
                        "Word": "點解",
                        "Offset": 97900000,
                        "Duration": 2000000
                    },
                    {
                        "Word": "要",
                        "Offset": 100000000,
                        "Duration": 1300000
                    },
                    {
                        "Word": "咁",
                        "Offset": 101400000,
                        "Duration": 2100000
                    },
                    {
                        "Word": "做",
                        "Offset": 103600000,
                        "Duration": 1700000
                    },
                    {
                        "Word": "喇",
                        "Offset": 105400000,
                        "Duration": 2300000
                    },
                    {
                        "Word": "因為",
                        "Offset": 107800000,
                        "Duration": 2300000
                    },
                    {
                        "Word": "已經",
                        "Offset": 110200000,
                        "Duration": 2000000
                    },
                    {
                        "Word": "有太",
                        "Offset": 112300000,
                        "Duration": 4000000
                    },
                    {
                        "Word": "多",
                        "Offset": 116400000,
                        "Duration": 1500000
                    },
                    {
                        "Word": "太多",
                        "Offset": 118000000,
                        "Duration": 3100000
                    },
                    {
                        "Word": "嘅",
                        "Offset": 121200000,
                        "Duration": 1100000
                    },
                    {
                        "Word": "研究",
                        "Offset": 122400000,
                        "Duration": 3900000
                    },
                    {
                        "Word": "顯示",
                        "Offset": 126400000,
                        "Duration": 3700000
                    },
                    {
                        "Word": "出",
                        "Offset": 130200000,
                        "Duration": 1600000
                    },
                    {
                        "Word": "咧",
                        "Offset": 131900000,
                        "Duration": 2900000
                    },
                    {
                        "Word": "演講者",
                        "Offset": 135100000,
                        "Duration": 5600000
                    },
                    {
                        "Word": "嘅",
                        "Offset": 140800000,
                        "Duration": 1500000
                    },
                    {
                        "Word": "非",
                        "Offset": 142400000,
                        "Duration": 2300000
                    },
                    {
                        "Word": "言語",
                        "Offset": 144800000,
                        "Duration": 4300000
                    },
                    {
                        "Word": "表達",
                        "Offset": 149200000,
                        "Duration": 4200000
                    },
                    {
                        "Word": "咧",
                        "Offset": 153500000,
                        "Duration": 1000000
                    },
                    {
                        "Word": "其實",
                        "Offset": 154600000,
                        "Duration": 5500000
                    },
                    {
                        "Word": "嘅",
                        "Offset": 160200000,
                        "Duration": 1700000
                    },
                    {
                        "Word": "重要性",
                        "Offset": 162000000,
                        "Duration": 5600000
                    },
                    {
                        "Word": "呢",
                        "Offset": 167700000,
                        "Duration": 1000000
                    },
                    {
                        "Word": "係",
                        "Offset": 168800000,
                        "Duration": 2100000
                    },
                    {
                        "Word": "畀",
                        "Offset": 171000000,
                        "Duration": 4400000
                    },
                    {
                        "Word": "語調",
                        "Offset": 175700000,
                        "Duration": 4100000
                    },
                    {
                        "Word": "啦",
                        "Offset": 179900000,
                        "Duration": 1600000
                    },
                    {
                        "Word": "甚至",
                        "Offset": 181600000,
                        "Duration": 2500000
                    },
                    {
                        "Word": "我哋",
                        "Offset": 184200000,
                        "Duration": 2100000
                    },
                    {
                        "Word": "一般",
                        "Offset": 186400000,
                        "Duration": 2900000
                    },
                    {
                        "Word": "以為",
                        "Offset": 189400000,
                        "Duration": 2500000
                    },
                    {
                        "Word": "嘅",
                        "Offset": 192000000,
                        "Duration": 1000000
                    },
                    {
                        "Word": "演講",
                        "Offset": 193100000,
                        "Duration": 3800000
                    },
                    {
                        "Word": "內容",
                        "Offset": 197000000,
                        "Duration": 4700000
                    },
                    {
                        "Word": "嚟",
                        "Offset": 201800000,
                        "Duration": 2200000
                    },
                    {
                        "Word": "得",
                        "Offset": 204100000,
                        "Duration": 1400000
                    },
                    {
                        "Word": "更為",
                        "Offset": 205600000,
                        "Duration": 3300000
                    },
                    {
                        "Word": "重要",
                        "Offset": 209000000,
                        "Duration": 4400000
                    },
                    {
                        "Word": "嘅",
                        "Offset": 213500000,
                        "Duration": 3000000
                    },
                    {
                        "Word": "咁",
                        "Offset": 216800000,
                        "Duration": 2200000
                    },
                    {
                        "Word": "我哋",
                        "Offset": 219100000,
                        "Duration": 2200000
                    },
                    {
                        "Word": "識別",
                        "Offset": 221400000,
                        "Duration": 2900000
                    },
                    {
                        "Word": "到",
                        "Offset": 224400000,
                        "Duration": 1300000
                    },
                    {
                        "Word": "啲",
                        "Offset": 225800000,
                        "Duration": 1100000
                    },
                    {
                        "Word": "咩",
                        "Offset": 227000000,
                        "Duration": 1700000
                    },
                    {
                        "Word": "咧",
                        "Offset": 228800000,
                        "Duration": 2500000
                    },
                    {
                        "Word": "譬如",
                        "Offset": 231600000,
                        "Duration": 3700000
                    },
                    {
                        "Word": "自然",
                        "Offset": 235400000,
                        "Duration": 3700000
                    },
                    {
                        "Word": "狀態",
                        "Offset": 239200000,
                        "Duration": 3500000
                    },
                    {
                        "Word": "勒",
                        "Offset": 242800000,
                        "Duration": 3200000
                    }
                ],
                "Display": "我哋呢個網站咧，其實係利用咗人工智能入邊嘅機器，學習，去幫我哋進行一個肢體語言嘅識別嘅，咁我哋點解要咁做喇？因為已經有太多太多嘅研究，顯示出咧演講者嘅非言語表達咧，其實嘅重要性呢係畀語調啦，甚至我哋一般以為嘅演講內容嚟得更為重要嘅，咁我哋識別到啲咩咧？譬如自然狀態勒？",
                "Lexical": "我哋 呢個 網站 咧 其實 係 利用 咗 人工智能 入邊 嘅 機器 學習 去 幫 我哋 進行 一個 肢體語言 嘅 識別 嘅 咁 我哋 點解 要 咁 做 喇 因為 已經 有太 多 太多 嘅 研究 顯示 出 咧 演講者 嘅 非 言語 表達 咧 其實 嘅 重要性 呢 係 畀 語調 啦 甚至 我哋 一般 以為 嘅 演講 內容 嚟 得 更為 重要 嘅 咁 我哋 識別 到 啲 咩 咧 譬如 自然 狀態 勒",
                "MaskedITN": "我哋呢個網站咧其實係利用咗人工智能入邊嘅機器學習去幫我哋進行一個肢體語言嘅識別嘅咁我哋點解要咁做喇因為已經有太多太多嘅研究顯示出咧演講者嘅非言語表達咧其實嘅重要性呢係畀語調啦甚至我哋一般以為嘅演講內容嚟得更為重要嘅咁我哋識別到啲咩咧譬如自然狀態勒",
                "Confidence": 0.67287785
            },
            {
                "ITN": "我 覺得 上次 用 手部 動作 去 配合 演講 呢 呢件 係 好事 啦 但系 相反 咯 喎 如果 演講者 咧 橋 受 或者 受 魔 受 呢一 啲 係 防禦性 動作 嚟嘅 可能 佢 就想 賴 弦 同 其他 嘅 距離 佢 而家 有啲 緊張 啦 又 或者 佢 好不 安 呀 就係 魔鏡 啦 或者 凹頭 啦 佢 想 利用 自己 捉摸 自己 嘅 呢啲 自細 咧 去 進行 自我 安慰 嘅 咁 又 或者 最差 嘅 情況 啦 好 啦 佢 爬頭 佢 啱啱 可能 講 錯 嘢 有 一啲 實在 哎呀 咁樣",
                "Words": [
                    {
                        "Word": "我",
                        "Offset": 252200000,
                        "Duration": 500000
                    },
                    {
                        "Word": "覺得",
                        "Offset": 252800000,
                        "Duration": 2000000
                    },
                    {
                        "Word": "上次",
                        "Offset": 254900000,
                        "Duration": 2900000
                    },
                    {
                        "Word": "用",
                        "Offset": 257900000,
                        "Duration": 1400000
                    },
                    {
                        "Word": "手部",
                        "Offset": 259400000,
                        "Duration": 3700000
                    },
                    {
                        "Word": "動作",
                        "Offset": 263200000,
                        "Duration": 3300000
                    },
                    {
                        "Word": "去",
                        "Offset": 266600000,
                        "Duration": 1100000
                    },
                    {
                        "Word": "配合",
                        "Offset": 267800000,
                        "Duration": 2800000
                    },
                    {
                        "Word": "演講",
                        "Offset": 270700000,
                        "Duration": 3600000
                    },
                    {
                        "Word": "呢",
                        "Offset": 274400000,
                        "Duration": 900000
                    },
                    {
                        "Word": "呢件",
                        "Offset": 275400000,
                        "Duration": 3500000
                    },
                    {
                        "Word": "係",
                        "Offset": 279000000,
                        "Duration": 900000
                    },
                    {
                        "Word": "好事",
                        "Offset": 280000000,
                        "Duration": 3900000
                    },
                    {
                        "Word": "啦",
                        "Offset": 284000000,
                        "Duration": 3900000
                    },
                    {
                        "Word": "但系",
                        "Offset": 288200000,
                        "Duration": 3100000
                    },
                    {
                        "Word": "相反",
                        "Offset": 291400000,
                        "Duration": 4200000
                    },
                    {
                        "Word": "咯",
                        "Offset": 295700000,
                        "Duration": 800000
                    },
                    {
                        "Word": "喎",
                        "Offset": 296600000,
                        "Duration": 1700000
                    },
                    {
                        "Word": "如果",
                        "Offset": 298400000,
                        "Duration": 2400000
                    },
                    {
                        "Word": "演講者",
                        "Offset": 300900000,
                        "Duration": 5200000
                    },
                    {
                        "Word": "咧",
                        "Offset": 306200000,
                        "Duration": 2800000
                    },
                    {
                        "Word": "橋",
                        "Offset": 309300000,
                        "Duration": 3000000
                    },
                    {
                        "Word": "受",
                        "Offset": 312400000,
                        "Duration": 6700000
                    },
                    {
                        "Word": "或者",
                        "Offset": 319400000,
                        "Duration": 3100000
                    },
                    {
                        "Word": "受",
                        "Offset": 322600000,
                        "Duration": 1900000
                    },
                    {
                        "Word": "魔",
                        "Offset": 324600000,
                        "Duration": 1500000
                    },
                    {
                        "Word": "受",
                        "Offset": 326200000,
                        "Duration": 2200000
                    },
                    {
                        "Word": "呢一",
                        "Offset": 328500000,
                        "Duration": 5300000
                    },
                    {
                        "Word": "啲",
                        "Offset": 333900000,
                        "Duration": 800000
                    },
                    {
                        "Word": "係",
                        "Offset": 334800000,
                        "Duration": 1700000
                    },
                    {
                        "Word": "防禦性",
                        "Offset": 336600000,
                        "Duration": 5900000
                    },
                    {
                        "Word": "動作",
                        "Offset": 342600000,
                        "Duration": 4300000
                    },
                    {
                        "Word": "嚟嘅",
                        "Offset": 347000000,
                        "Duration": 3300000
                    },
                    {
                        "Word": "可能",
                        "Offset": 350400000,
                        "Duration": 3300000
                    },
                    {
                        "Word": "佢",
                        "Offset": 353800000,
                        "Duration": 1900000
                    },
                    {
                        "Word": "就想",
                        "Offset": 355800000,
                        "Duration": 4500000
                    },
                    {
                        "Word": "賴",
                        "Offset": 360400000,
                        "Duration": 2100000
                    },
                    {
                        "Word": "弦",
                        "Offset": 362600000,
                        "Duration": 1700000
                    },
                    {
                        "Word": "同",
                        "Offset": 364400000,
                        "Duration": 2500000
                    },
                    {
                        "Word": "其他",
                        "Offset": 367000000,
                        "Duration": 3800000
                    },
                    {
                        "Word": "嘅",
                        "Offset": 370900000,
                        "Duration": 2200000
                    },
                    {
                        "Word": "距離",
                        "Offset": 373200000,
                        "Duration": 3500000
                    },
                    {
                        "Word": "佢",
                        "Offset": 376800000,
                        "Duration": 1100000
                    },
                    {
                        "Word": "而家",
                        "Offset": 378000000,
                        "Duration": 2100000
                    },
                    {
                        "Word": "有啲",
                        "Offset": 380200000,
                        "Duration": 2300000
                    },
                    {
                        "Word": "緊張",
                        "Offset": 382600000,
                        "Duration": 4400000
                    },
                    {
                        "Word": "啦",
                        "Offset": 387100000,
                        "Duration": 5100000
                    },
                    {
                        "Word": "又",
                        "Offset": 392600000,
                        "Duration": 2500000
                    },
                    {
                        "Word": "或者",
                        "Offset": 395200000,
                        "Duration": 2500000
                    },
                    {
                        "Word": "佢",
                        "Offset": 397800000,
                        "Duration": 2300000
                    },
                    {
                        "Word": "好不",
                        "Offset": 400200000,
                        "Duration": 4300000
                    },
                    {
                        "Word": "安",
                        "Offset": 404600000,
                        "Duration": 1900000
                    },
                    {
                        "Word": "呀",
                        "Offset": 406600000,
                        "Duration": 5300000
                    },
                    {
                        "Word": "就係",
                        "Offset": 413000000,
                        "Duration": 2700000
                    },
                    {
                        "Word": "魔鏡",
                        "Offset": 415800000,
                        "Duration": 5000000
                    },
                    {
                        "Word": "啦",
                        "Offset": 420900000,
                        "Duration": 5000000
                    },
                    {
                        "Word": "或者",
                        "Offset": 426200000,
                        "Duration": 3900000
                    },
                    {
                        "Word": "凹頭",
                        "Offset": 430200000,
                        "Duration": 4700000
                    },
                    {
                        "Word": "啦",
                        "Offset": 435000000,
                        "Duration": 2100000
                    },
                    {
                        "Word": "佢",
                        "Offset": 437200000,
                        "Duration": 2100000
                    },
                    {
                        "Word": "想",
                        "Offset": 439400000,
                        "Duration": 1900000
                    },
                    {
                        "Word": "利用",
                        "Offset": 441400000,
                        "Duration": 3800000
                    },
                    {
                        "Word": "自己",
                        "Offset": 445300000,
                        "Duration": 3400000
                    },
                    {
                        "Word": "捉摸",
                        "Offset": 448800000,
                        "Duration": 3300000
                    },
                    {
                        "Word": "自己",
                        "Offset": 452200000,
                        "Duration": 3300000
                    },
                    {
                        "Word": "嘅",
                        "Offset": 455600000,
                        "Duration": 900000
                    },
                    {
                        "Word": "呢啲",
                        "Offset": 456600000,
                        "Duration": 3300000
                    },
                    {
                        "Word": "自細",
                        "Offset": 460000000,
                        "Duration": 4900000
                    },
                    {
                        "Word": "咧",
                        "Offset": 465000000,
                        "Duration": 1500000
                    },
                    {
                        "Word": "去",
                        "Offset": 466600000,
                        "Duration": 1900000
                    },
                    {
                        "Word": "進行",
                        "Offset": 468600000,
                        "Duration": 3500000
                    },
                    {
                        "Word": "自我",
                        "Offset": 472200000,
                        "Duration": 4500000
                    },
                    {
                        "Word": "安慰",
                        "Offset": 476800000,
                        "Duration": 3900000
                    },
                    {
                        "Word": "嘅",
                        "Offset": 480800000,
                        "Duration": 6800000
                    },
                    {
                        "Word": "咁",
                        "Offset": 490000000,
                        "Duration": 5300000
                    },
                    {
                        "Word": "又",
                        "Offset": 495400000,
                        "Duration": 1300000
                    },
                    {
                        "Word": "或者",
                        "Offset": 496800000,
                        "Duration": 3100000
                    },
                    {
                        "Word": "最差",
                        "Offset": 500000000,
                        "Duration": 3400000
                    },
                    {
                        "Word": "嘅",
                        "Offset": 503500000,
                        "Duration": 1500000
                    },
                    {
                        "Word": "情況",
                        "Offset": 505100000,
                        "Duration": 3700000
                    },
                    {
                        "Word": "啦",
                        "Offset": 508900000,
                        "Duration": 1000000
                    },
                    {
                        "Word": "好",
                        "Offset": 510000000,
                        "Duration": 1400000
                    },
                    {
                        "Word": "啦",
                        "Offset": 511500000,
                        "Duration": 1000000
                    },
                    {
                        "Word": "佢",
                        "Offset": 512600000,
                        "Duration": 2100000
                    },
                    {
                        "Word": "爬頭",
                        "Offset": 514800000,
                        "Duration": 8700000
                    },
                    {
                        "Word": "佢",
                        "Offset": 523800000,
                        "Duration": 2700000
                    },
                    {
                        "Word": "啱啱",
                        "Offset": 526600000,
                        "Duration": 2500000
                    },
                    {
                        "Word": "可能",
                        "Offset": 529200000,
                        "Duration": 2300000
                    },
                    {
                        "Word": "講",
                        "Offset": 531600000,
                        "Duration": 2100000
                    },
                    {
                        "Word": "錯",
                        "Offset": 533800000,
                        "Duration": 1900000
                    },
                    {
                        "Word": "嘢",
                        "Offset": 535800000,
                        "Duration": 2100000
                    },
                    {
                        "Word": "有",
                        "Offset": 538000000,
                        "Duration": 900000
                    },
                    {
                        "Word": "一啲",
                        "Offset": 539000000,
                        "Duration": 2100000
                    },
                    {
                        "Word": "實在",
                        "Offset": 541200000,
                        "Duration": 2700000
                    },
                    {
                        "Word": "哎呀",
                        "Offset": 544000000,
                        "Duration": 5500000
                    },
                    {
                        "Word": "咁樣",
                        "Offset": 549600000,
                        "Duration": 5400000
                    }
                ],
                "Display": "我覺得上次用手部動作去配合演講呢，呢件係好事啦，但系相反咯喎。如果演講者咧橋受或者受魔受呢？一啲係防禦性動作嚟嘅，可能佢就想賴弦同其他嘅距離佢而家有啲緊張啦，又或者佢好不安呀，就係魔鏡啦。或者凹頭啦，佢想利用自己捉摸自己嘅呢啲自細咧去進行自我安慰嘅咁又或者最差嘅情況啦。好啦，佢爬頭，佢啱啱可能講錯嘢，有一啲實在。哎呀，咁樣。",
                "Lexical": "我 覺得 上次 用 手部 動作 去 配合 演講 呢 呢件 係 好事 啦 但系 相反 咯 喎 如果 演講者 咧 橋 受 或者 受 魔 受 呢一 啲 係 防禦性 動作 嚟嘅 可能 佢 就想 賴 弦 同 其他 嘅 距離 佢 而家 有啲 緊張 啦 又 或者 佢 好不 安 呀 就係 魔鏡 啦 或者 凹頭 啦 佢 想 利用 自己 捉摸 自己 嘅 呢啲 自細 咧 去 進行 自我 安慰 嘅 咁 又 或者 最差 嘅 情況 啦 好 啦 佢 爬頭 佢 啱啱 可能 講 錯 嘢 有 一啲 實在 哎呀 咁樣",
                "MaskedITN": "我覺得上次用手部動作去配合演講呢呢件係好事啦但系相反咯喎如果演講者咧橋受或者受魔受呢一啲係防禦性動作嚟嘅可能佢就想賴弦同其他嘅距離佢而家有啲緊張啦又或者佢好不安呀就係魔鏡啦或者凹頭啦佢想利用自己捉摸自己嘅呢啲自細咧去進行自我安慰嘅咁又或者最差嘅情況啦好啦佢爬頭佢啱啱可能講錯嘢有一啲實在哎呀咁樣",
                "Confidence": 0.6728778
            },
            {
                "ITN": "一啲 字 細 喇 我哋 都係 會 跌 到 嘅",
                "Words": [
                    {
                        "Word": "一啲",
                        "Offset": 556500000,
                        "Duration": 5100000
                    },
                    {
                        "Word": "字",
                        "Offset": 561700000,
                        "Duration": 1900000
                    },
                    {
                        "Word": "細",
                        "Offset": 563700000,
                        "Duration": 1700000
                    },
                    {
                        "Word": "喇",
                        "Offset": 565500000,
                        "Duration": 800000
                    },
                    {
                        "Word": "我哋",
                        "Offset": 566400000,
                        "Duration": 2100000
                    },
                    {
                        "Word": "都係",
                        "Offset": 568600000,
                        "Duration": 2300000
                    },
                    {
                        "Word": "會",
                        "Offset": 571000000,
                        "Duration": 1600000
                    },
                    {
                        "Word": "跌",
                        "Offset": 572700000,
                        "Duration": 4200000
                    },
                    {
                        "Word": "到",
                        "Offset": 577000000,
                        "Duration": 1800000
                    },
                    {
                        "Word": "嘅",
                        "Offset": 578900000,
                        "Duration": 3200000
                    }
                ],
                "Display": "一啲字細喇，我哋都係會跌到嘅。",
                "Lexical": "一啲 字 細 喇 我哋 都係 會 跌 到 嘅",
                "MaskedITN": "一啲字細喇我哋都係會跌到嘅",
                "Confidence": 0.67287785
            }
        ]), end_of_speech_offset: 609000000,
        user_id: 1,
        topic_id: 13
    }]);

    await knex(tables.VIDEOS).insert([{
        file_name: '1626603558412',
        topic_id: 1,
    }, {
        file_name: '1626808380088',
        topic_id: 13,
    }]);

}
