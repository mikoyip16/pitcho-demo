import { Knex } from 'knex';
import { hashPassword } from '../hash';
const usersTableName = 'users';

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex(usersTableName).del();

  // Inserts seed entries
  await knex(usersTableName).insert([
    { username: 'tecky', password: await hashPassword('tecky'), email: 'tecky@tecky.io' },
    {
      username: 'alan',
      password: await hashPassword('alan'),
      email: 'alan@tecky.io',
    },
    {
      username: 'mary',
      password: await hashPassword('mary'),
      email: 'mary@tecky.io',
    }
  ]);
}
