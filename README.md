

## Setup .env file according to .env.sample



## Install ffmpeg for converting video to wav file

    -   from this website: https://www.ffmpeg.org/download.html
    -   unzip the folder
    -   go to `bin` folder inside the unzipped folder
    -   create a `bin` folder in your project folder, set to .gitignore
    -   copy `ffmpeg.exe` to the `bin` folder

## Install word2vec packages

    -   pip install jieba
    -   pip install -U gensim OR conda install gensim
    -   brew install opencc OR pip install opencc
    -   pip install fastapi uvicorn
    -   uvicorn demo:app --reload


## Database set up


1. CREATE DATABASE
2. create tables

```Bash
yarn knex migrate:latest
```

3. run seed files by this order:

```Bash
yarn knex seed:run --specific=create-test-user.ts
yarn knex seed:run --specific=init-pitchTopic.ts
yarn knex seed:run --specific=init-training.ts
yarn knex seed:run --specific=demo-record.ts
```
## Demo user account

-   username: tecky
-   password: tecky