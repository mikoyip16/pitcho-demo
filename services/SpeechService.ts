import { Knex } from 'knex';
import { tables } from '../tables';
import * as nodejieba from 'nodejieba';
import translate from 'google-translate-open-api';
import fs, { readFileSync } from 'fs';
import os from 'os';
const axios = require('axios');
import {
    AudioConfig,
    CancellationReason,
    ResultReason,
    SpeechConfig,
    SpeechRecognizer,
    OutputFormat,
} from 'microsoft-cognitiveservices-speech-sdk';
import dotenv from 'dotenv';
import ffmpeg from 'fluent-ffmpeg';
import path from 'path';

dotenv.config();


export class SpeechService {
    constructor(private knex: Knex) { }

    async checkSimilarity(topic: string, paragraph: string) {

        const simpifiedChinese = await translate(paragraph, { 
            to: 'zh-cn', 
            client: 'dict-chrome-ex',
        });

        let scParagraph = simpifiedChinese.data.sentences.map((x: { trans: any }) => x.trans).join(''); //call個google translate api係一句一句call的，所以要join返佢做一段


        const scWordsArray = nodejieba.cut(scParagraph); 

        let scWordsStr = scWordsArray.join(',');
        const traditionalChinese = await translate(scWordsStr, {
            tld: 'com',
            to: 'zh-tw', 
            client: 'dict-chrome-ex',
        });
        let tcParagraph = traditionalChinese.data.sentences
            .map((x: { trans: any }) => x.trans)
            .join(''); 
        let tcWordsArray = tcParagraph.split(',');


        const file = readFileSync('jieba_dict/stopwords.txt', 'utf-8');
        const stopWordArray = file.split('\n'); 
        let stopwordsInParagragraph = [];
        let result: any[] = [];
        let relatedWords: number = 0;

        for (const word of tcWordsArray) {
            if (stopWordArray.includes(word)) {
                relatedWords++;
                stopwordsInParagragraph.push(word);
            } else {
                result.push(
                    (await axios.get(encodeURI(`http://localhost:8000/check?topic=${topic}&word=${word}`)))
                        .data
                ); 


            }
        }

        const resultRelevant = result.filter(obj => {
            if (parseFloat(obj.similarity) > 0.09) return true
            else return false
        })

        const resultIrrelevant = result.filter(obj => {
            if (parseFloat(obj.similarity) > 0.09) return false
            else return true
        })



        return { resultRelevant, resultIrrelevant, relevance: (relatedWords / tcWordsArray.length) * 100 * 2 + 'Scores' }; //!!!!!!!而家計個貼題分係就咁將佢*2
    }

    async formatToWav(inputFile: string, outputFile: string, timestamp: number, userID: number, topicID: number) {
        const type = os.type();

        switch (type) {
            case 'Darwin':
                ffmpeg.setFfmpegPath('./bin/ffmpeg');
                break;
            case 'Linux':
                ffmpeg.setFfmpegPath('/usr/bin/ffmpeg');
                break;
            case 'Windows_NT':
                ffmpeg.setFfmpegPath('./bin/ffmpeg');
                break;
        }

        const ffmpegCommand = ffmpeg(inputFile);

        ffmpegCommand
            .noVideo()
            .audioFilters(['loudnorm=I=-16:TP=-1.5:LRA=7'])
            .audioBitrate('256k')
            .audioCodec('pcm_s16le')
            .output(outputFile)
            .on('start', function (ffmpegCommand) {
                console.log('Running the ffmpeg command: ', ffmpegCommand);
            })
            .on('end', () => {
                console.log('file converted');

                this.speechToText(outputFile, timestamp, userID, topicID);
            })
            .run();
    }

    async speechToText(file: string, timestamp: number, userID: number, topicID: number) {
        const speechConfig = SpeechConfig.fromSubscription(
            process.env.SDK_API_KEY as string,
            process.env.SDK_API_REGION as string
        );
        speechConfig.enableDictation();
        speechConfig.speechRecognitionLanguage = 'zh-HK';
        speechConfig.requestWordLevelTimestamps();
        speechConfig.outputFormat = OutputFormat.Detailed;

        let audioConfig = AudioConfig.fromWavFileInput(fs.readFileSync(file));

        let recognizer = new SpeechRecognizer(speechConfig, audioConfig);

        

        let transcriptDetails: {}[] = [];
        let endOfSpeechOffset: number;

        recognizer.recognized = (s, e) => {
            if (e.result.reason == ResultReason.RecognizedSpeech) {
                console.log('working on it...');

                const result = JSON.parse(e.result.json);
                const topResult = result['NBest'][0];
                if (topResult['Words'] !== undefined) {
                    transcriptDetails.push(topResult);
                }
            } else if (e.result.reason == ResultReason.NoMatch) {
                console.log('NOMATCH: Speech could not be recognized.');
            }
        };

        recognizer.speechEndDetected = (s, e) => {
            endOfSpeechOffset = e.offset;
        };

        recognizer.canceled = (s, e) => {
            if (e.reason == CancellationReason.EndOfStream) {
                console.log('End of stream');
            }

            if (e.reason == CancellationReason.Error) {
                console.log(`CANCELED: ErrorCode=${e.errorCode}`);
                console.log(`CANCELED: ErrorDetails=${e.errorDetails}`);
                console.log('CANCELED: Did you update the key and location/region info?');
            }

            recognizer.stopContinuousRecognitionAsync(
                async () => {
                    console.log('stop recogizing');
                    recognizer.close();
                    await this.knex(tables.REPORT)
                        .insert({
                            video_filename: timestamp,
                            hesitation_details: JSON.stringify(transcriptDetails),
                            end_of_speech_offset: endOfSpeechOffset,
                            user_id: userID,
                            topic_id: topicID
                        })
                        .returning('id');
                },
                (err) => {
                    console.error(err);
                    recognizer.close();
                }
            );
        };

        recognizer.startContinuousRecognitionAsync(
            () => {
                console.log('start recognizing...');
            },
            (err) => {
                console.log(err);
                recognizer.close();
            }
        );
    }
}
