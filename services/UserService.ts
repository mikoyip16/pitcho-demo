import { Knex } from 'knex';
import { hashPassword } from '../hash';
import { Users } from '../models';
import { tables } from '../tables';

export class UserService {
  constructor(private knex: Knex) {}

  async login(emailOrUsername: string) {
    const foundUser = await this.knex<Users>(tables.USERS)
      .where('username', emailOrUsername)
      .orWhere('email', emailOrUsername)
      .first();
    return foundUser;
  }

  async createUser(username: string, email: string, password: string) {
    const hashedPassword = await hashPassword(password);
    await this.knex(tables.USERS).insert({
      username: username,
      email: email,
      password: hashedPassword,
    });
  }
}
