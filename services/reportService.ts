import { Knex } from 'knex';
import { tables } from '../tables';

export class ReportService {
  constructor(private knex: Knex) {}

  async getText(videoID: number) {
    const result = await this.knex
      .select('id', 'hesitation_details', 'end_of_speech_offset')
      .from(tables.REPORT)
      .where('id', videoID);
    console.log(result);
    return result;
  }

  async getAllReport(userID: number) {
    const result = await this.knex
      .select('pitch_topic.topic', 'report.id', 'username', 'report.created_at')
      .from('report')
      .innerJoin('users', 'report.user_id', 'users.id')
      .innerJoin('pitch_topic', 'report.topic_id', 'pitch_topic.pitch_topic_id')
      .where('users.id', userID);
    return result;
  }

  async getPastReportSingle(reportID: number) {
    const result = await this.knex
      .select('video_filename', 'text', 'hesitation_details', 'end_of_speech_offset')
      .from(tables.REPORT)
      .where('id', reportID);
    return result;
  }
}
