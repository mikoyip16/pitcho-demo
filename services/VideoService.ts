import { Knex } from 'knex';
import { Video } from '../models';
import { tables } from '../tables';

export class VideoService {
  constructor(private knex: Knex) {}

  async createVideo(videoFileName: string, topicID: number) {
    const [insertedId] = await this.knex(tables.VIDEOS)
      .insert({ file_name: videoFileName, topic_id: topicID })
      .returning('id');
    return insertedId as number;
  }

  async getVideo(videoID: number) {
    const foundVideo = (await this.knex<Video>(tables.VIDEOS).select().where('id', videoID))[0];
    return foundVideo;
  }

  async getPastVideo(reportID: number) {
    const foundVideo = (await this.knex<Video>(tables.VIDEOS).select().where('id', reportID))[0];
    return foundVideo;

  }
}
