# -*- coding: utf-8 -*-

from gensim.models import word2vec
from gensim import models
from fastapi import FastAPI
import logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
model = models.Word2Vec.load('word2vec.model')

app = FastAPI()
@app.get("/check")
async def main(topic: str, word: str):

	# print("提供 3 種測試模式\n")
	# print("輸入一個詞，則去尋找前一百個該詞的相似詞")
	# print("輸入兩個詞，則去計算兩個詞的餘弦相似度")
	# print("輸入三個詞，進行類比推理")
    try:
        res = model.wv.similarity(topic,word)
        return({"message": "Success","word": word ,"similarity": '%.5f' % res})
    except KeyError as e:
        return({"message":"Topic/ word not exist in data", "word": word})